
import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import GeoJSON from 'ol/format/GeoJSON.js';
import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style.js';

var image = new CircleStyle({
  radius: 2,
  fill: null,
  stroke: new Stroke({color: 'red', width: 2})
});

var pointStyle = new Style({
  image: image
});


var vectorLayer = new VectorLayer({
    source: new VectorSource({
      url: 'http://localhost:5000/api/gis/Points',
      format: new GeoJSON()
    }),
    style: pointStyle

  });

var backgroundMap = new TileLayer({
    source: new OSM()
  });

const map = new Map({
  target: 'map',
  projection: 'EPSG:3857',
  layers: [
      backgroundMap,
      vectorLayer   
  ],
  view: new View({
    center: [2217805, 6461355],
    zoom: 14
  })
});

console.log(map)
console.log(vectorLayer)
console.log(map.projection)
