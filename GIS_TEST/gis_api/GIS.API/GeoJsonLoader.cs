using System.Collections.Generic;
using GIS.API.DTOs;
using GIS.API.GeoJsonApi;
using Newtonsoft.Json;

namespace GIS.API
{
    public class GeoJsonLoader
    {
        public GeoJsonRoot GeoJsonRoot { get; set; }
        public string GeoJsonRootSerialized {get; set;}

        public void LoadSpatialData(List<SpatialFeature> features, bool withSpatialProjection)
        {
            GeoJsonRoot geoJsonRoot = new GeoJsonRoot();
            geoJsonRoot.type = "FeatureCollection";

            foreach (SpatialFeature spatialFeature in features)
            {
                if (withSpatialProjection)
                {
                    SpatialFeature projectedSpatialFeature = CoordinateTools.ProjectSpatialFeature(spatialFeature);
                    geoJsonRoot.features.Add(projectedSpatialFeature);
                } else
                {
                    geoJsonRoot.features.Add(spatialFeature);
                }
                
            }

            GeoJsonRoot = geoJsonRoot;
            GeoJsonRootSerialized = JsonConvert.SerializeObject(geoJsonRoot);
            
        }
    }
}