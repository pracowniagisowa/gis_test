using GIS.API.GeoJsonApi;

namespace GIS.API.DTOs
{
    public class TrackPointFeature : SpatialFeature
    {
        public TrackPointFeature()
        {
            base.properties = new TrackPointsProperties();
            base.geometry = new PointGeometry();     
        }
    }
}