namespace GIS.API.DTOs 
{
    public class TrackPointsDataBase : SpatialEntity
    {
        public TrackPointsDataBase (int id, double x, double y) 
            : base(x, y)
        {
            this.Id = id;
        }
        public int Id { get; set; }
        // public double X { get; set; }
        // public double Y { get; set; }
    }
}