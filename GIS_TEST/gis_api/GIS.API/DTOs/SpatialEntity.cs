namespace GIS.API.DTOs
{
    public abstract class SpatialEntity
    {
        public double X { get; set; }
        public double Y { get; set; }

        public SpatialEntity(double x, double y)
        {
            X = x;
            Y = y;
        }
    }
}