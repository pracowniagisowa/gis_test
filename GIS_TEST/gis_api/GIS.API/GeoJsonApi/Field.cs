namespace GIS.API.GeoJsonApi
{
    public class Field
    {
        public string name { get; set; }
        public string type { get; set; }
        public string alias { get; set; }
        public int? length { get; set; }
    }
}