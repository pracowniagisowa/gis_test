﻿namespace GIS.API.GeoJsonApi
{
    public class SpatialFeature
    {
        public string type { get; set; } = "Feature";
        public Properties properties { get; set; }
        public Geometry geometry { get; set; }
    }
}