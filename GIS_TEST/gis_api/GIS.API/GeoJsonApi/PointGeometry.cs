using System.Collections.Generic;

namespace GIS.API.GeoJsonApi {
    public class PointGeometry : Geometry 
    {
        public List<double> coordinates { get; set; }

        public PointGeometry()
            :base("Point")
        {
            coordinates = new List<double>();
        }
        public PointGeometry (List<double> coordinates) 
            :base("Point")
        {
            this.coordinates = coordinates;
        }
    }
}