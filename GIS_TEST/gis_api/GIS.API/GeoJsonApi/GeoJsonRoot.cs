using System.Collections.Generic;

namespace GIS.API.GeoJsonApi
{
    public class GeoJsonRoot
    {
        public string type { get; set; }
        // public string objectIdFieldName { get; set; }
        // public string geometryType { get; set; }
        // public SpatialReference spatialReference { get; set; }
        // public List<Field> fields { get; set; }
        public List<SpatialFeature> features { get; set; }

        public GeoJsonRoot()
        {
            features = new List<SpatialFeature>();
        }
    }
}