using System.Collections.Generic;

namespace GIS.API.GeoJsonApi {
    public class MultiPointGeometry : Geometry {
        public List<List<double>> Points { get; set; }

        public MultiPointGeometry (List<List<double>> points) 
            : base("MultiPoint")
        {
            this.Points = points;           

        }
    }
}