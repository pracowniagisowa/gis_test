namespace GIS.API.GeoJsonApi
{
    public abstract class Geometry
    {
        public Geometry(string type)
        {
            this.type = type;
        }

        public string type { get; set; }
    }
}