namespace GIS.API.GeoJsonApi
{
    public class SpatialReference
    {
        public int wkid { get; set; }
        public int latestWkid { get; set; }
    }
}