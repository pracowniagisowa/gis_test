using System;
using System.Collections.Generic;
using GIS.API.DTOs;
using GIS.API.Enums;
using GIS.API.GeoJsonApi;

namespace GIS.API
{
    public class CoordinateTools
    {
        const double RadiansPerDegree = Math.PI / 180;

        public static List<double> ProjectPointToWebMercator(double x, double y)
        {
            double Rad = x * RadiansPerDegree;
            double FSin = Math.Sin(Rad);
            double DegreeEqualsRadians = 0.017453292519943;
            double EarthsRadius = 6378137;
     
            double webY = EarthsRadius / 2.0 * Math.Log((1.0 + FSin) / (1.0 - FSin));
            double webX = x * DegreeEqualsRadians * EarthsRadius;

            return new List<double>{webX, webY};
        }

        // public static List<MapPoint> ProjectListToWebMercator(List<MapPoint> mapPoints){
        //     List<MapPoint> projectedCoordinates = new List<MapPoint>();
        //     foreach (MapPoint mapPoint in mapPoints)
        //     {
        //         projectedCoordinates.Add(ProjectPointToWebMercator(mapPoint));
        //     }
        //     return projectedCoordinates;
        // }

        public static SpatialFeature ProjectSpatialFeature(SpatialFeature spatialFeature)
        {   
            SpatialFeature projectedSpatialFeature = new SpatialFeature();
            projectedSpatialFeature.properties = spatialFeature.properties;
            
            switch (spatialFeature.geometry.type)
            {
                case("Point"):
                {
                    PointGeometry pointGeometry = spatialFeature.geometry as PointGeometry;
                    if (pointGeometry != null)
                    {
                        projectedSpatialFeature.geometry = new PointGeometry(ProjectPointToWebMercator(
                            pointGeometry.coordinates[0],
                            pointGeometry.coordinates[1]
                        ));
                    }                    
                    break;
                }
                default:
                    break;
            }

            return projectedSpatialFeature;
        }
    }
}