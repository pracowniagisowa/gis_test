using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GIS.API.Controllers
{   
    // [Route("api/[controller]/")]
    [ApiController]
    public class GisController : ControllerBase
    {
        [HttpGet]
        [Route("api/gis/points")]
        public ActionResult<string> Points()
        {
            GeoJsonLoader geoJsonLoader = new GeoJsonLoader();
            geoJsonLoader.LoadSpatialData(GeoData.GetTrackPointsFeatures(), false);


            var pointsJson = geoJsonLoader.GeoJsonRootSerialized;    
            return pointsJson;
        }
    }
}